**Food Order Web Application**

This project is based on docker.

In order to run the application for the first time, please follow the below steps:

1) download project

2) run: sudo docker-compose build

3) run: sudo docker-compose up

4) in a new terminal tab run: sudo docker exec -it foodorder-database bash

5) run: cd /usr/src/sql

6) run: mysql -u root -p

7) password is 123456

8) run: source foodorder_db.sql;

There you go!

You can find API Document and Postman Collection of API in the documents folder.