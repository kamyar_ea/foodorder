<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{

    public function register(Request $request)
    {

        $email = $request->email;
        $full_name = $request->full_name;
        $password = $request->password;

        $validator = Validator::make(['full_name'=>$full_name],['full_name'=>'required|max:20']);

        if ($validator->fails()){
            return response()->json(['message'=>'full_name is required and should be maximum 20 characters'],422);
        }

        $validator = Validator::make(['email'=>$email],['email'=>'required|email']);

        if ($validator->fails()){
            return response()->json(['message'=>'email is required and should be a valid email'],422);
        }

        $validator = Validator::make(['email'=>$email],['email'=>'unique:users,email']);

        if ($validator->fails()){
            return response()->json(['message'=>'email is used'],422);
        }

        $validator = Validator::make(['password'=>$password],['password'=>'required']);

        if ($validator->fails()){
            return response()->json(['message'=>'password is required'],422);
        }

        $validator = Validator::make(['password'=>$password],['password'=>'min:8']);

        if ($validator->fails()){
            return response()->json(['message'=>'password should be minimum 8 characters'],422);
        }

        $user = New User;

        $user->email = $email;
        $user->name = $full_name;
        $user->password = bcrypt($password);

        $user->save();

        return response()->json(['message'=>'registration was successful'],200);

    }

    public function login(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $validator = Validator::make(['email'=>$email],['email'=>'required|email|string']);

        if ($validator->fails()){
            return response()->json(['message'=>'please enter a valid email'],422);
        }

        $validator = Validator::make(['password'=>$password],['password'=>'required|string']);

        if ($validator->fails()){
            return response()->json(['message'=>'password is required'],422);
        }

        $user = User::where('email',$email)->first();

        if (count($user)>0){

            $credentials = [
                'email'=>$email,
                'password'=>$password
            ];

            if (!Auth::attempt($credentials)){
                return response()->json(['message'=>'wrong email or password'],401);
            }

            $token = $user->createToken('Token')->accessToken;

            return response()->json([
                'access_token'=>$token,
                'token_type'=>'Bearer'
            ],200);

        } else {

            return response()->json(['message'=>'wrong email or password'],401);

        }

    }

    public function invalid_token()
    {
        return response()->json(['message'=>'invalid token'],401);
    }
    
}
