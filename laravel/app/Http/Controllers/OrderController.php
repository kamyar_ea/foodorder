<?php

namespace App\Http\Controllers;

use App\Order;
use App\Order_food;
use App\Order_state;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;

class OrderController extends Controller
{

    public function order_state_list()
    {

        $states = Order_state::get(['state_id','name']);

        return response()->json(['result'=>$states],200);

    }

    public function order_add(Request $request)
    {

        $restaurant_id = $request->restaurant_id;
        $state_id = $request->state_id;
        $food_ids = $request->food_ids;
        $food_ids = json_decode($food_ids);

        $validator = Validator::make(['food_ids'=>$food_ids],['food_ids'=>'required|array']);

        if ($validator->fails()){
            return response()->json(['message'=>'food_ids is required and should be json string'],422);
        }

        $validator = Validator::make(['restaurant_id'=>$restaurant_id],['restaurant_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'restaurant_id is required and should be integer'],422);
        }

        $validator = Validator::make(['state_id'=>$state_id],['state_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'state_id is required and should be integer'],422);
        }

        $user_id = $request->user()->id;

        $order = New Order;

        $order->restaurant_id = $restaurant_id;
        $order->state_id = $state_id;
        $order->created_by = $user_id;

        $order->save();

        $order_id = $order->id;

        $now = Carbon::now()->toDateTimeString();

        $food_ids_array = [];

        foreach ($food_ids as $food_id){

            $food_ids_array[] = [
                'order_id'=>$order_id,
                'food_id'=>$food_id,
                'created_by'=>$user_id,
                'created_at'=>$now,
                'updated_at'=>$now
            ];

        }

        Order_food::insert($food_ids_array);

        return response()->json(['message'=>'order was submitted'],200);

    }

    public function order_list(Request $request)
    {

        $user_id = $request->user()->id;

        $orders = Order::with(['foods:id,name','restaurant:id,name','state:id,name'])
            ->where([['orders.created_by','=',$user_id],['orders.is_deleted','=',0]])
            ->get(['orders.id','orders.restaurant_id','orders.state_id']);

        return response()->json(['result'=>$orders],200);

    }

    public function order_detail(Request $request)
    {

        $order_id = $request->order_id;

        $validator = Validator::make(['order_id'=>$order_id],['order_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'order_id is required and should be integer'],422);
        }

        $user_id = $request->user()->id;

        $order = Order::with(['foods:id,name','restaurant:id,name','state:id,name'])
            ->where([['orders.id','=',$order_id],['orders.created_by','=',$user_id],['orders.is_deleted','=',0]])
            ->get(['orders.id','orders.restaurant_id','orders.state_id']);

        return response()->json(['result'=>$order],200);

    }

    public function order_edit(Request $request)
    {

        $order_id = $request->order_id;
        $restaurant_id = $request->restaurant_id;
        $food_ids = $request->food_ids;
        $food_ids = json_decode($food_ids);

        $validator = Validator::make(['order_id'=>$order_id],['order_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'order_id is required and should be integer'],422);
        }

        $validator = Validator::make(['food_ids'=>$food_ids],['food_ids'=>'required|array']);

        if ($validator->fails()){
            return response()->json(['message'=>'food_ids is required and should be json string'],422);
        }

        $validator = Validator::make(['restaurant_id'=>$restaurant_id],['restaurant_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'restaurant_id is required and should be integer'],422);
        }

        $user_id = $request->user()->id;

        $order = Order::find($order_id);

        if (count($order)){

            $order->restaurant_id = $restaurant_id;

            $order->save();

            $now = Carbon::now()->toDateTimeString();

            Order_food::where('order_id', $order_id)->delete();

            $food_ids_array = [];

            foreach ($food_ids as $food_id){

                $food_ids_array[] = [
                    'order_id'=>$order_id,
                    'food_id'=>$food_id,
                    'created_by'=>$user_id,
                    'created_at'=>$now,
                    'updated_at'=>$now
                ];

            }

            Order_food::insert($food_ids_array);

            return response()->json(['message'=>'order was edited'],200);

        } else {

            return response()->json(['message'=>'order was not found'],422);

        }

    }

    public function order_delete(Request $request)
    {

        $order_id = $request->order_id;

        $validator = Validator::make(['order_id'=>$order_id],['order_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'order_id is required and should be integer'],422);
        }

        $order = Order::where([['id','=',$order_id],['is_deleted','=',0]])->first();

        if (count($order)){

            $order->is_deleted = 1;

            $order->save();

            return response()->json(['message'=>'order was deleted'],200);

        } else {

            return response()->json(['message'=>'order was not found'],422);

        }

    }

    public function order_change_state(Request $request)
    {

        $order_id = $request->order_id;
        $state_id = $request->state_id;

        $validator = Validator::make(['order_id'=>$order_id],['order_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'order_id is required and should be integer'],422);
        }

        $validator = Validator::make(['state_id'=>$state_id],['state_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'state_id is required and should be integer'],422);
        }

        $order = Order::where([['id','=',$order_id],['is_deleted','=',0]])->first();

        if (count($order)){

            $order->state_id = $state_id;

            $order->save();

            return response()->json(['message'=>'order state was changed'],200);

        } else {

            return response()->json(['message'=>'order was not found'],422);

        }

    }

}
