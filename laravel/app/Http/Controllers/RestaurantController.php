<?php

namespace App\Http\Controllers;

use App\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class RestaurantController extends Controller
{

    public function restaurant_add(Request $request)
    {

        $name = $request->name;

        $validator = Validator::make(['name'=>$name],['name'=>'required|max:50']);

        if ($validator->fails()){
            return response()->json(['message'=>'restaurant name is required and should be maximum 50 characters'],422);
        }

        $restaurant = new Restaurant;

        $restaurant->name = $name;
        $restaurant->created_by = $request->user()->id;

        $restaurant->save();

        return response()->json(['message'=>'restaurant was added'],200);

    }

    public function restaurant_list()
    {

        $restaurants = Restaurant::get(['id','name','created_by']);
        return response()->json(['result'=>$restaurants],200);

    }

}
