<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use App\Food;


class FoodController extends Controller
{

    public function food_add(Request $request)
    {

        $restaurant_id = $request->restaurant_id;
        $foods = $request->food_names;
        $foods = json_decode($foods);

        $validator = Validator::make(['foods'=>$foods],['foods'=>'required|array']);

        if ($validator->fails()){
            return response()->json(['message'=>'foods is required and should be json string'],422);
        }

        $validator = Validator::make(['restaurant_id'=>$restaurant_id],['restaurant_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'restaurant_id is required and should be integer'],422);
        }

        $user_id = $request->user()->id;

        $now = Carbon::now()->toDateTimeString();

        $foods_array = [];

        foreach ($foods as $food){
            $foods_array[] = [
                'name'=>$food,
                'restaurant_id'=>$restaurant_id,
                'created_by'=>$user_id,
                'created_at'=>$now,
                'updated_at'=>$now
            ];
        }

        Food::insert($foods_array);

        return response()->json(['message'=>'foods were added'],200);

    }

    public function food_list(Request $request)
    {

        $restaurant_id = $request->restaurant_id;

        $validator = Validator::make(['restaurant_id'=>$restaurant_id],['restaurant_id'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'restaurant_id is required and should be integer'],422);
        }

        $foods = Food::where('restaurant_id',$restaurant_id)->get(['id','name']);

        return response()->json(['result'=>$foods],200);

    }

}
