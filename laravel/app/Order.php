<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    public function foods()
    {
        return $this->belongsToMany('App\Food','order_food');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }

    public function state()
    {
        return $this->belongsTo('App\Order_state');
    }

}
