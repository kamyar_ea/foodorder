<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register','UserController@register');
Route::post('/login','UserController@login');
Route::get('/invalid_token','UserController@invalid_token')->name('invalid_token');

Route::middleware('auth:api')->group(function () {

    Route::post('/restaurant_add','RestaurantController@restaurant_add');
    Route::get('/restaurant_list','RestaurantController@restaurant_list');

    Route::post('/food_add','FoodController@food_add');
    Route::post('/food_list','FoodController@food_list');

    Route::get('/order_state_list','OrderController@order_state_list');
    Route::post('/order_add','OrderController@order_add');
    Route::get('/order_list','OrderController@order_list');
    Route::post('/order_detail','OrderController@order_detail');
    Route::post('/order_edit','OrderController@order_edit');
    Route::post('/order_delete','OrderController@order_delete');
    Route::post('/order_change_state','OrderController@order_change_state');

});