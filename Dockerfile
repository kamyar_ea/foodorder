FROM php:7.1-apache
RUN docker-php-ext-install pdo_mysql
WORKDIR /var/www
COPY ./laravel ./laravel
COPY ./public/. ./html/
RUN chown -R www-data:www-data /var/www
RUN a2enmod rewrite
RUN apt-get update && apt-get install -y mysql-client
WORKDIR /var/www/laravel

EXPOSE 80